import random


def dice(mini: int = 1, maxi: int = 6) -> int:
    value = random.randint(mini, maxi)
    print(f"Jet Dée : {value}")
    return value


def dice_100(mini: int = 1) -> int:
    return dice(mini, 100)


def dice_20(mini: int = 1) -> int:
    return dice(mini, 20)


def play_dice(objectif: int, mini: int = 1, maxi: int = 6) -> bool:
    if dice(mini, maxi) >= objectif:
        print("Jet réussi")
        return True
    print("Jet raté")
    return False


def play_dice_100(objectif: int, mini: int = 1) -> bool:
    return play_dice(objectif, mini, 100)


def play_dice_20(objectif: int, mini: int = 1) -> int:
    return play_dice(objectif, mini, 20)
