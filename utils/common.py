import threading
from waiting import wait


def timeout(seconds):
    def decorator(func):
        def wrapper(*args, **kwargs):
            result = [Exception("La fonction n'a pas réussi a s'executer avant le delai imparti !")]

            # définit une fonction intermédiaire pour exécuter la fonction originale en arrière-plan
            def run():
                try:
                    result[0] = func(*args, **kwargs, event_timer=event)
                except Exception as e:
                    result[0] = e
                event.clear()

            event = threading.Event()
            thread = threading.Thread(target=run)
            thread.start()
            thread.join(timeout=seconds)

            # Si le thread est encore actif, cela signifie que le temps imparti est écoulé
            # Je passe donc mon event à set pour prévenir la fonction que le timer est finit
            if thread.is_alive():
                event.set()
                wait(lambda: not event.is_set(), timeout_seconds=10)
                raise TimeoutError(
                    "Temps imparti ecoule ! "
                    "La fonction n'a pas reussi a s'executer avant le delai imparti."
                )

            # si la fonction a levé une exception, elle est levée ici dans le thread principal
            if isinstance(result[0], Exception):
                raise result[0]

            # si la fonction s'est exécutée correctement, elle retourne la valeur de retour ici
            return result[0]

        return wrapper

    return decorator


def retry(time):
    def decorator(func):
        def wrapper(*args, **kwargs):
            retry_time = 0
            while retry_time < time:
                retry_time += 1
                print(f"Nombre d'essaie : {retry_time}", flush=True)
                try:
                    result = func(*args, **kwargs)
                    return result
                except Exception as e:
                    print(f"{e}\n")
            return None

        return wrapper

    return decorator
