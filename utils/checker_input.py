def input_string(prompt: str = "", minimal: int = 2, maximal: int = 32) -> str:
    my_input = input(prompt)
    while not my_input.isalpha() or len(my_input) < minimal or maximal < len(my_input):
        print(f"Bad input : str entre {minimal} et {maximal} charactere")
        my_input = input(prompt)
    return my_input


def input_int(prompt: str = "", minimal: int = 0, maximal: int = 10000000000000) -> int:
    while True:
        try:
            my_input = int(input(prompt))
            if maximal >= my_input >= minimal:
                return my_input
            raise ValueError
        except ValueError:
            print(f"Bad input : int entre {minimal} et {maximal}")
