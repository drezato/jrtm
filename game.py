"""My beautiful game :)"""
import sys
import codecs
from scenario.initialisation_partie import InitialisationPartie

if __name__ == "__main__":
    sys.stdout.reconfigure(encoding="utf-8")
    if sys.stdout.encoding != "UTF-8":
        if sys.version_info[0] < 3:
            sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
    InitialisationPartie()
