"""Interface générique et abstractive pour les attributs d'un personnage (comme langue, carac,
comp, sort)."""
from abc import ABC, abstractmethod

from utils.checker_input import input_int


class InterfacePersonnage(ABC):
    PRETTY_CHOOSE_TXT = ""

    @property
    @abstractmethod
    def list_properties(self) -> list:
        pass

    def upgrade_attribute_with_selection_point(self, points: int):
        while points > 0:
            print(f"Vous avez {points} points additionnels")
            selected_nb_point = self._upgrade_select_points(points)
            selected_attribute = self._upgrade_select_attribute()
            self._upgrade_attribute(selected_attribute, selected_nb_point)
            points -= selected_nb_point

    def upgrade_attribute_without_selection_point(self, points):
        self._upgrade_attribute(self._upgrade_select_attribute(), points)

    @staticmethod
    def _upgrade_select_points(points: int) -> int:
        """Choisir combien de point à mettre à l'attribut."""
        print("Choissisez combien de point vous voulez utiliser")
        if points == 1:
            return 1
        return input_int(minimal=1, maximal=points)

    def _upgrade_select_attribute(self) -> object:
        """Choisir quelle attribut le joueur veut selectionner."""
        count = 0
        for element in self.list_properties:
            print(f"[{count}] {element.name}")
            count += 1
        return self.list_properties[input_int(maximal=len(self.list_properties) - 1)]

    def _upgrade_attribute(self, attributes: object, points: int = 1):
        """Rajoute le nombre de point à l'attribut en question."""
        attributes += points
        print(self)
