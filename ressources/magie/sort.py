from dataclasses import dataclass


@dataclass
class Sort:
    nom: str = None
    type: str = None
    niveau: int = 0
    air_effet: int = 0
    duree: int = 0
    portee: int = 0

    # Sort                                           //EXEMPLE
    # protected $sort['name']['name'] = 0;           //Eclair de glace
    # protected $sort['name']['classe_de_sort'] = 0; //E (élémentaire)
    # protected $sort['name']['type'] = 0;           //combat
    # protected $sort['name']['sous_type'] = 0;      //eclair
    # protected $sort['name']['royaume'] = 0;        //essence
    # protected $sort['name']['name_liste'] = 0;     //Les voies du froid
    # protected $sort['name']['portee'] = 0;         //30 (metre)
    # protected $sort['name']['niveau'] = 0;         //6
    # protected $sort['name']['element'] = 0;        //froid
    # protected $sort['name']['duree'] = 0;          //-
    # protected $sort['name']['charge'] = 0 = 0;     //true      //Si c'est un sort a chargé ou pas
    # protected $sort['name']['air_effet'] = 0 = 0;  //1
