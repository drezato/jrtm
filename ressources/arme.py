from dataclasses import dataclass


@dataclass
class Arme:
    nom: str = None
    cout: int = 0
    poids: int = 0
    abreviation: str = None

    statistique_maladresse: int = 0
    statistique_coupcritprimaire: int = 0
    statistique_coupcritsecondaire: int = 0
    statistique_modifspeciaux: int = 0
    statistique_enchantement: int = 0
