from pprint import pprint

from db.db import DB

from ressources.creature.langue.langue import Langue
from ressources.creature.basedefensif import BaseDefensif
from ressources.creature.baseoffensif import BaseOffensif
from ressources.creature.resistance import ResistanceCreature
from ressources.creature.personnage.caracteristique import CaracteristiqueCreature


class Creature(DB):
    def __init__(self, name: str = None):
        super().__init__()
        self.name: str = name
        self.caracteristiques: CaracteristiqueCreature = CaracteristiqueCreature()

        self.type_arme: str = ""
        self.type_armure: str = ""
        self.base_offensif: BaseOffensif = BaseOffensif()
        self.base_defensif: BaseDefensif = BaseDefensif(caracteristiques=self.caracteristiques)

        # 1 pj, 2 monstre & animaux, 3 pnj
        self.type_creature: int = 0

        # Resistance
        self.resistances: ResistanceCreature = ResistanceCreature(caracteristiques=self.caracteristiques)

        self.langue: Langue = Langue()

        self.point_de_vie: int = 0
        self.point_de_vie_max: int = 1

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        pprint(vars(self))
        return ""
