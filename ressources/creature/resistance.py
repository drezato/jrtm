from pprint import pprint

from dataclasses import dataclass

from ressources.creature.personnage.caracteristique import CaracteristiqueCreature, Caracteristique


@dataclass
class Resistance:
    name: str
    race: int = 0
    special_1: int = 0

    caracteristique_primordial: Caracteristique = None

    @property
    def carac(self):
        return self.caracteristique_primordial.total

    @property
    def total(self):
        return self.carac + self.race + self.special_1


class ResistanceCreature:
    def __init__(self, caracteristiques: CaracteristiqueCreature):
        self.feu: Resistance = Resistance(
            name="Feu", caracteristique_primordial=caracteristiques.intelligence
        )
        self.eau: Resistance = Resistance(
            name="Eau", caracteristique_primordial=caracteristiques.intelligence
        )
        self.froid: Resistance = Resistance(
            name="Froid", caracteristique_primordial=caracteristiques.intelligence
        )
        self.lumiere: Resistance = Resistance(
            name="Lumière", caracteristique_primordial=caracteristiques.intuition
        )
        self.obscurite: Resistance = Resistance(
            name="Obscurité", caracteristique_primordial=caracteristiques.intuition
        )

        self.essence: Resistance = Resistance(
            name="Essence", caracteristique_primordial=caracteristiques.intelligence
        )
        self.theurgie: Resistance = Resistance(
            name="Theurgie", caracteristique_primordial=caracteristiques.intuition
        )
        self.poison: Resistance = Resistance(
            name="poison", caracteristique_primordial=caracteristiques.constitution
        )
        self.maladie: Resistance = Resistance(
            name="Maladie", caracteristique_primordial=caracteristiques.constitution
        )

    @property
    def list_properties(self) -> list[Resistance]:
        return [
            self.feu,
            self.eau,
            self.froid,
            self.lumiere,
            self.obscurite,
            self.essence,
            self.theurgie,
            self.poison,
            self.maladie,
        ]

    def __repr__(self):
        return str(vars(self))

    def __str__(self):
        pprint(vars(self))
        return ""
