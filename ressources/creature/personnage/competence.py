from dataclasses import dataclass

from ressources.interface_personnage import InterfacePersonnage
from ressources.creature.personnage.caracteristique import CaracteristiqueCreature, Caracteristique


@dataclass
class Competence:
    name: str
    niveau: int = 1
    experience_total: int = 0

    degre: int = -25
    objet: int = 0
    special_1: int = 0
    special_2: int = 0

    caracteristique_primordial: Caracteristique = None

    @property
    def carac(self) -> int:
        return self.caracteristique_primordial.total

    @property
    def total(self) -> int:
        return self.degre + self.carac + self.special_1 + self.special_2

    def add_point_degre(self):
        if self.degre == -25:
            self.degre += 30
        elif self.degre < 50:
            self.degre += 5
        elif self.degre < 100:
            self.degre += 2
        else:
            self.degre += 1

    def __str__(self):
        return f"degre: {self.degre} - carac: {self.carac} - objet: {self.objet} - special_1: {self.special_1} - special_2: {self.special_2} - total: {self.total}   -- niveau: {self.niveau} -  experience: {self.experience_total}"


class CompetencePersonnage(InterfacePersonnage):
    PRETTY_CHOOSE_TXT = "Choisissez une compétence à apprendre :)"

    def __init__(self, caracteristiques: CaracteristiqueCreature):
        self.manoeuvreetmouvement_sansarmure: object = Competence(
            "Armure : sans armure", caracteristique_primordial=caracteristiques.agilite
        )
        self.manoeuvreetmouvement_cuirsouple: object = Competence(
            "Armure : cuir souple", special_2=-15, caracteristique_primordial=caracteristiques.agilite
        )
        self.manoeuvreetmouvement_cuirrigide: object = Competence(
            "Armure : cuir rigide", special_2=-30, caracteristique_primordial=caracteristiques.agilite
        )
        self.manoeuvreetmouvement_cottedemaille: object = Competence(
            "Armure : cotte de maille", special_2=-40, caracteristique_primordial=caracteristiques.force
        )
        self.manoeuvreetmouvement_plate: object = Competence(
            "Armure : plate", special_2=-60, caracteristique_primordial=caracteristiques.force
        )

        self.arme_tranchantunemain: object = Competence(
            "Arme : tranchant à une main", caracteristique_primordial=caracteristiques.force
        )
        self.arme_contondantunemain: object = Competence(
            "Arme : contondant à une main", caracteristique_primordial=caracteristiques.force
        )
        self.arme_deuxmains: object = Competence(
            "Arme : à deux mains", caracteristique_primordial=caracteristiques.force
        )
        self.arme_armedelance: object = Competence(
            "Arme : arme de lancé", caracteristique_primordial=caracteristiques.agilite
        )
        self.arme_projectile: object = Competence(
            "Arme : projectile", caracteristique_primordial=caracteristiques.agilite
        )
        self.arme_armedhast: object = Competence(
            "Arme : hast", caracteristique_primordial=caracteristiques.force
        )

        self.generale_escalade: object = Competence(
            "Générale : escalade", caracteristique_primordial=caracteristiques.agilite
        )
        self.generale_equitation: object = Competence(
            "Générale : équitation", caracteristique_primordial=caracteristiques.intuition
        )
        self.generale_natation: object = Competence(
            "Générale : natation", caracteristique_primordial=caracteristiques.agilite
        )
        self.generale_pistage: object = Competence(
            "Générale : pistage", caracteristique_primordial=caracteristiques.intelligence
        )

        self.subterfuge_embuscade: object = Competence(
            "Subterfuge : embuscade", caracteristique_primordial=caracteristiques.agilite
        )
        self.subterfuge_filatdissim: object = Competence(
            "Subterfuge : filature/dissimulation", caracteristique_primordial=caracteristiques.presence
        )
        self.subterfuge_crochetage: object = Competence(
            "Subterfuge : crochetage", caracteristique_primordial=caracteristiques.intelligence
        )
        self.subterfuge_desarmementdepiege: object = Competence(
            "Subterfuge : desarmement de piège", caracteristique_primordial=caracteristiques.intuition
        )

        self.magie_lecturederune: object = Competence(
            "Magie : lecture de rune", caracteristique_primordial=caracteristiques.intelligence
        )
        self.magie_utilisationdobjet: object = Competence(
            "Magie : utilisation d'objet", caracteristique_primordial=caracteristiques.intuition
        )
        self.magie_directiondesort: object = Competence(
            "Magie : direction de sort", caracteristique_primordial=caracteristiques.agilite
        )

        self.physique_developcorporel: object = Competence(
            "Physique : developpement corporel (point de vie)",
            degre=0,
            special_2=5,
            caracteristique_primordial=caracteristiques.constitution,
        )
        self.physique_perception: object = Competence(
            "Physique : perception", caracteristique_primordial=caracteristiques.intuition
        )

    @property
    def list_properties(self) -> list[Competence]:
        return [
            self.manoeuvreetmouvement_sansarmure,
            self.manoeuvreetmouvement_cuirsouple,
            self.manoeuvreetmouvement_cuirrigide,
            self.manoeuvreetmouvement_cottedemaille,
            self.manoeuvreetmouvement_plate,
            self.arme_tranchantunemain,
            self.arme_contondantunemain,
            self.arme_deuxmains,
            self.arme_armedelance,
            self.arme_projectile,
            self.arme_armedhast,
            self.generale_escalade,
            self.generale_equitation,
            self.generale_natation,
            self.generale_pistage,
            self.subterfuge_embuscade,
            self.subterfuge_filatdissim,
            self.subterfuge_crochetage,
            self.subterfuge_desarmementdepiege,
            self.magie_lecturederune,
            self.magie_utilisationdobjet,
            self.magie_directiondesort,
            self.physique_developcorporel,
            self.physique_perception,
        ]

    def _upgrade_attribute(self, competence: Competence, points: int = 1):
        for _ in range(points):
            competence.add_point_degre()
        print(self)
        print("")

    def __repr__(self):
        return str(vars(self))

    def __str__(self):
        return "\n".join(f"[{competence.name}]  ==>  {competence}" for competence in self.list_properties)
