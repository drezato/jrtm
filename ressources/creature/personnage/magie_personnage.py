from dataclasses import dataclass

from db.db import DB
from utils.dice import play_dice_100
from utils.checker_input import input_int
from ressources.interface_personnage import InterfacePersonnage


@dataclass
class ListSort:
    nom: str = None
    type: str = None
    acquis: bool = False
    pourcentage_apprentissage: int = 0


class MagiePersonnage(InterfacePersonnage):
    PRETTY_CHOOSE_TXT = "Choisissez une liste de sort à apprendre :)"

    def __init__(self):
        # Array avec des listes ex : = [les voies du froid, les liaisons supérieurs, ...]
        self.list_sort: list[ListSort] = [
            ListSort(nom=sort["nom"], type=sort["type"]) for sort in DB().select("list_sort")
        ]

    @property
    def list_properties(self) -> list:
        return []

    def _upgrade_select_attribute(self) -> ListSort:
        count = 0
        for listsort in self.list_sort:
            if listsort.acquis:
                print(f"[{count}] {listsort.nom} (déjà acquis)")
            else:
                print(f"[{count}] {listsort.nom} (acquis à {listsort.pourcentage_apprentissage}%)")
            count += 1

        while True:
            print("Choisissez une liste de sort à apprendre :)")
            selected_list = self.list_sort[input_int(maximal=count - 1)]
            if not selected_list.acquis:
                return selected_list
            print("Vous connaissez déjà cette list de sort...")

    def _upgrade_attribute(self, list_sort: ListSort, points: int = 100):
        list_sort.pourcentage_apprentissage += points
        if play_dice_100(100 - list_sort.pourcentage_apprentissage):
            list_sort.acquis = True
            list_sort.pourcentage_apprentissage = 100
        print(self)
        print("")

    def __str__(self):
        return (
            f"List de sort acquis = {' - '.join(sort.nom for sort in self.list_sort if sort.acquis)}\n"
            f"List de sort en apprentissage = {' - '.join(f'{sort.nom} ({sort.pourcentage_apprentissage}%)' for sort in self.list_sort if 0 < sort.pourcentage_apprentissage < 100)}"
        )
