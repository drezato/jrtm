from ressources.creature.personnage.personnage_joueur.race.race import Race
from ressources.creature.personnage.personnage_joueur.personnage_joueur import PersonnageJoueur


class ElfeNoldor(Race, PersonnageJoueur):
    @property
    def race_name(self):
        return "Elfe Noldor"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.langue.adunaic.value = 3
        self.langue.quenya.value = 5
        self.langue.sindarin.value = 5
        self.langue.westron.value = 5

        self.caracteristiques.agilite.race = 15
        self.caracteristiques.constitution.race = 10
        self.caracteristiques.intelligence.race = 5
        self.caracteristiques.intuition.race = 5
        self.caracteristiques.presence.race = 15

        self.resistances.poison.race = 10
        self.resistances.maladie.race = 100

        self.competences.manoeuvreetmouvement_sansarmure.degre = 5

        self.competences.arme_tranchantunemain.degre = 5
        self.competences.arme_projectile.degre = 5

        self.competences.generale_equitation.degre = 5
        self.competences.generale_natation.degre = 10
        self.competences.generale_pistage.degre = 5

        self.competences.subterfuge_filatdissim.degre = 10

        self.competences.magie_lecturederune.degre = 10
        self.competences.magie_utilisationdobjet.degre = 5

        self.competences.physique_developcorporel.degre = 5
        self.competences.physique_perception.degre = 15

        self.points_histoire = 4
        self.degres_langage_additionnel = 1
        self.chance_obtenir_liste_sort_pourcentage = 40
