from ressources.creature.personnage.personnage_joueur.race.race import Race
from ressources.creature.personnage.personnage_joueur.personnage_joueur import PersonnageJoueur


class Nain(Race, PersonnageJoueur):
    @property
    def race_name(self):
        return "Nain"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.langue.khuzdul.value = 5
        self.langue.westron.value = 5
        self.langue.sindarin.value = 3

        self.caracteristiques.force.race = 5
        self.caracteristiques.agilite.race = -5
        self.caracteristiques.constitution.race = 15
        self.caracteristiques.intuition.race = -5
        self.caracteristiques.presence.race = -5

        self.resistances.poison.race = 10
        self.resistances.maladie.race = 10
        self.resistances.essence.race = 40

        self.competences.manoeuvreetmouvement_sansarmure.degre = 5
        self.competences.manoeuvreetmouvement_cuirrigide.degre = 15
        self.competences.manoeuvreetmouvement_cottedemaille.degre = 15

        self.competences.arme_contondantunemain.degre = 20
        self.competences.arme_armedelance.degre = 5

        self.competences.generale_escalade.degre = 5

        self.competences.subterfuge_crochetage.degre = 5
        self.competences.subterfuge_desarmementdepiege.degre = 5

        self.competences.physique_developcorporel.degre = 15
        self.competences.physique_perception.degre = 10

        self.points_histoire = 4
        self.degres_langage_additionnel = 4
        self.chance_obtenir_liste_sort_pourcentage = 3
