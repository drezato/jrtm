"""Abstract Race class for typing."""
from abc import ABC, abstractmethod


class Race(ABC):
    @property
    @abstractmethod
    def race_name(self):
        pass
