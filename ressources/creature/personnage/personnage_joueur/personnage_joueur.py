from typing import Union
from dataclasses import dataclass

from utils.dice import dice_100

from ressources.creature.personnage.personnage import Personnage
from ressources.creature.personnage.magie_personnage import MagiePersonnage
from ressources.creature.personnage.personnage_joueur.race.race import Race


@dataclass
class PowerPoint:
    caracteristique_value: int
    bonus: int


class PersonnageJoueur(Personnage):
    POWER_POINT_RULES_LIST = [
        PowerPoint(74, 0),
        PowerPoint(94, 1),
        PowerPoint(99, 2),
        PowerPoint(101, 3),
        PowerPoint(102, 4),
        PowerPoint(999, 5),
    ]

    def __init__(
        self,
        *args,
        taille: int = 0,
        age: int = 0,
        poids: int = 0,
        cheveux: str = "",
        yeux: str = "",
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.race: Union[Race, None] = None
        self.taille: int = taille
        self.age: int = age
        self.poids: int = poids
        self.cheveux: str = cheveux
        self.yeux: str = yeux
        self.signeparticulier: str = ""
        self.penalitedencombrement: int = 0

        self.piece: int = 20000

        self.type_creature: int = 1

        self.magie: MagiePersonnage = MagiePersonnage()

        self.points_histoire: int = 0
        self.degres_langage_additionnel: int = 0
        self.chance_obtenir_liste_sort_pourcentage: int = 0
        self.point_de_pouvoir_actuelle: int = 0

    @property
    def point_de_pouvoir_max(self) -> int:
        for power_point in self.POWER_POINT_RULES_LIST:
            if self.caracteristiques.intelligence.value <= power_point.caracteristique_value:
                return power_point.bonus
        raise IndexError

    def add_piece(self, niveau: int):
        """Add une valeur random de pièce au joueur en fonction d'un 'niveau' et d'un lancé de dée."""
        # RANDOM_PIECE = (PIECE, DICE)
        RANDOM_PIECE = [
            (1, 25),
            (4, 40),
            (8, 60),
            (20, 80),
            (50, 90),
            (75, 94),
            (100, 96),
            (125, 98),
            (150, 99),
            (200, 100),
        ]
        dice = dice_100()
        piece = 200
        for element in RANDOM_PIECE:
            if dice <= element[1]:
                piece = element[0]
                break
        self.piece += piece * niveau
        self.print_piece_earned(piece * niveau)

    def print_actual_player_piece(self):
        print(f"Vous avez {self._format_piece(self.piece)}")

    def print_piece_earned(self, quantity_piece: int):
        """Affiche combien le joueur vient de gagner d'or."""
        print(f"Vous venez de gagner {self._format_piece(quantity_piece)}")

    @staticmethod
    def _format_piece(quantity_piece: int):
        po = 0
        pa = 0
        pb = 0

        while quantity_piece > 0:
            if quantity_piece >= 10000:
                po = quantity_piece // 10000
                quantity_piece -= po * 10000
            if quantity_piece >= 100:
                pa = quantity_piece // 100
                quantity_piece -= pa * 100
            pb = quantity_piece
            quantity_piece -= pb

        return f"{po} pièce d'or, {pa} pièce d'argent, {pb} pièce de bronze"
