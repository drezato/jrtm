from ressources.arme import Arme
from ressources.armure import Armure
from ressources.creature.creature import Creature

from ressources.creature.personnage.competence import CompetencePersonnage


class Personnage(Creature):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.competences: object = CompetencePersonnage(caracteristiques=self.caracteristiques)

        self.arme: object = Arme()
        self.armure: object = Armure()
