from dataclasses import dataclass

from ressources.interface_personnage import InterfacePersonnage


@dataclass
class NormBonus:
    caracteristique_value: int
    bonus: int


@dataclass
class Caracteristique:
    NORM_RULES_LIST = [
        NormBonus(caracteristique_value=1, bonus=-25),
        NormBonus(caracteristique_value=2, bonus=-20),
        NormBonus(caracteristique_value=4, bonus=-15),
        NormBonus(caracteristique_value=9, bonus=-10),
        NormBonus(caracteristique_value=24, bonus=-5),
        NormBonus(caracteristique_value=74, bonus=0),
        NormBonus(caracteristique_value=89, bonus=5),
        NormBonus(caracteristique_value=94, bonus=10),
        NormBonus(caracteristique_value=97, bonus=15),
        NormBonus(caracteristique_value=99, bonus=20),
        NormBonus(caracteristique_value=100, bonus=25),
        NormBonus(caracteristique_value=101, bonus=30),
        NormBonus(caracteristique_value=999, bonus=35),
    ]

    name: str
    value: int = 25
    race: int = 0

    @property
    def norm(self) -> int:
        for norm_rule in self.NORM_RULES_LIST:
            if self.value <= norm_rule.caracteristique_value:
                return norm_rule.bonus
        raise IndexError

    @property
    def total(self) -> int:
        return self.norm + self.race

    def __str__(self):
        return f"value: {self.value} - race: {self.race} - norm: {self.norm} - total: {self.total}"


class CaracteristiqueCreature(InterfacePersonnage):
    PRETTY_CHOOSE_TXT = "Choissisez une caracteristique à apprendre :)"

    def __init__(self):
        self.force: Caracteristique = Caracteristique("Force")
        self.agilite: Caracteristique = Caracteristique("Agilité")
        self.constitution: Caracteristique = Caracteristique("Constitution")
        self.intelligence: Caracteristique = Caracteristique("Intelligence")
        self.intuition: Caracteristique = Caracteristique("Intuition")
        self.presence: Caracteristique = Caracteristique("Présence")

    @property
    def list_properties(self) -> list[Caracteristique]:
        return [self.force, self.agilite, self.constitution, self.intelligence, self.intuition, self.presence]

    def _upgrade_attribute(self, carac: Caracteristique, points: int = 1):
        carac.value += points
        print("Vos nouvelles caracteristiques :")
        print(self)
        print("")

    def __repr__(self):
        return str(vars(self))

    def __str__(self):
        return "\n".join(f"[{carac.name}]  ==>  {carac}" for carac in self.list_properties)
