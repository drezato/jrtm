from dataclasses import dataclass


@dataclass
class BaseOffensif:
    special_1: int = 0
    special_2: int = 0

    @property
    def total(self):
        return self.special_1 + self.special_2
