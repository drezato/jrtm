from dataclasses import dataclass

from utils.checker_input import input_int
from ressources.interface_personnage import InterfacePersonnage


@dataclass
class Language:
    name: str
    value: int = 0


class Langue(InterfacePersonnage):
    PRETTY_CHOOSE_TXT = "Choisissez un language à apprendre :)"

    def __init__(self):
        self.adunaic = Language("Adunaic")
        self.apysaic = Language("Apysaic")
        self.atliduk = Language("Atliduk")
        self.haradaic = Language("Haradaic")
        self.khuzdul = Language("Khuzdul")
        self.kuduk = Language("Kuduk")
        self.labba = Language("Labba")
        self.logathig = Language("Logathig")
        self.nahaiduk = Language("Nahaiduk")
        self.noirparler = Language("Noirparler")
        self.orque = Language("Orque")
        self.pukael = Language("Pukael")
        self.quenya = Language("Quenya")
        self.rohirric = Language("Rohirric")
        self.sindarin = Language("Sindarin")
        self.sylvain = Language("Sylvain")
        self.umitic = Language("Umitic")
        self.varadja = Language("Varadja")
        self.waildyth = Language("Waildyth")
        self.westron = Language("Westron", value=5)  # (Langage commun)

    @property
    def list_properties(self) -> list[Language]:
        return [
            self.adunaic,
            self.apysaic,
            self.atliduk,
            self.haradaic,
            self.khuzdul,
            self.kuduk,
            self.labba,
            self.logathig,
            self.nahaiduk,
            self.noirparler,
            self.orque,
            self.pukael,
            self.quenya,
            self.rohirric,
            self.sindarin,
            self.sylvain,
            self.umitic,
            self.varadja,
            self.waildyth,
            self.westron,
        ]

    def _upgrade_select_attribute(self) -> Language:
        count = 0
        for langue in self.list_properties:
            if langue.value == 5:
                print(f"[{count}] {langue.name} [5] (déjà acquis)")
            else:
                print(f"[{count}] {langue.name} [{langue.value}]")
            count += 1

        while True:
            selected_langue = self.list_properties[input_int(maximal=count - 1)]
            if selected_langue.value != 5:
                return selected_langue
            print("Vous ne pouvez pas selectionner un language déjà acquis parfaitement")

    def _upgrade_attribute(self, langue: Language, points: int = 1):
        langue.value = min(langue.value + points, 5)
        print(self)
        print("")

    def __repr__(self):
        return str(vars(self))

    def __str__(self):
        return "\n".join(f"[{langue.name}]  ==>  {langue.value}" for langue in self.list_properties)
