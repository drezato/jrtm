from dataclasses import dataclass

from ressources.creature.creature import Creature


@dataclass
class Monstre(Creature):
    niveau: int = 1
    nombre_apparition: int = 1
    vitesse: int = 1
    taille: int = 1
    critique: int = 1
