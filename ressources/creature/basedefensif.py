from ressources.creature.personnage.caracteristique import CaracteristiqueCreature


class BaseDefensif:
    def __init__(self, caracteristiques: CaracteristiqueCreature):
        self.special_1: int = 0
        self.special_2: int = 0

        self.caracteristiques: CaracteristiqueCreature = caracteristiques

    @property
    def carac(self) -> int:
        return self.caracteristiques.agilite.total

    @property
    def total(self) -> int:
        return self.carac + self.special_1 + self.special_2
