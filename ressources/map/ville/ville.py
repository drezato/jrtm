from dataclasses import dataclass, field

from ressources.map.instance import Instance


@dataclass
class Ville(Instance):
    evenement: list = field(default_factory=lambda: [])


CAPITAL = Ville(instance="Capital", taille=5, forme="carre", rgba=(255, 0, 0))
VILLE = Ville(instance="Ville", taille=4, forme="carre", rgba=(255, 100, 100))
VILLAGE = Ville(instance="Village", taille=3, forme="carre", rgba=(255, 180, 180))
