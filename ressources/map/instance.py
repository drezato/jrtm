from typing import Union
from dataclasses import dataclass, field


@dataclass
class Instance:
    instance: str = ""
    coordonne_x: int = 0
    coordonne_y: int = 0
    taille: Union[int, list[int]] = field(default_factory=lambda: [])
    nom: str = None
    type: str = "ville"
    forme: str = "linear"
    biome: list = field(default_factory=lambda: [])
    rgba: tuple = (70, 0, 0)


# Villes = Instance(instance="Villes", taille=[2, 10], type='ville', forme='linear', rgba=(70, 0, 0))
DONJONS = Instance(instance="Donjons", type="donjon", rgba=(70, 0, 0))
