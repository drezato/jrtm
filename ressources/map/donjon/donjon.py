from typing import Union
from dataclasses import dataclass, field

from db.db import DB
from ressources.map.instance import Instance

ALL_DONJON_DICT = DB().select(table="config_donjon")


@dataclass
class Donjon(Instance):
    evenement: list = field(default_factory=lambda: [])
    salle_boss: Union[bool, list] = False

    @staticmethod
    def list_donjon_biome(biome=None):
        if not biome:
            return ALL_DONJON_DICT

        biome_donjon = []
        for donjon in ALL_DONJON_DICT:
            if donjon["biome"] == biome:
                biome_donjon.append(donjon)

        return biome_donjon


# MOULIN = Donjon(instance="Donjons", taille=[4, 6], type="moulin", forme="linear", biome=[Biome.PLAINE, Biome.FORET])
# ARBRE_ENCHANTE = Donjon(instance="Donjons", taille=[3, 3], type="arbre_enchante", forme="carre", biome=[Biome.FORET_MAGIQUE, Biome.FORET])
# GROTTE = Donjon(instance="Donjons", taille=[2, 4], type="grotte", forme="carre", biome=[Biome.COLLINE, Biome.MONTAGNE])
# CAVERNE_PROFONDE = Donjon(instance="Donjons", taille=[5, 7], type="caverne_profonde", forme="linear", biome=[Biome.MONTAGNE_PROFONDE])
# CAVERNE = Donjon(instance="Donjons", taille=[6, 8], type="caverne", forme="linear", biome=[Biome.MONTAGNE])
# BATEAU = Donjon(instance="Donjons", taille=[2, 3], type="bateau", forme="carre", biome=[Biome.OCEAN_PROFOND, Biome.MER, Biome.OCEAN])
