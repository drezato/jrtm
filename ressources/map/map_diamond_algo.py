# pylint: disable=redefined-builtin, too-many-branches
import random
import numpy as np
import matplotlib.pyplot as mp


def generate_random_point(x, y):
    return round(random.uniform(x, y), 2)


def create_carte(h):
    reajusteur = 0

    h = 2**h + 1
    t = [[0] * h] * h
    t = np.array(t)
    t[0, 0] = generate_random_point(-h, h)
    t[0, h - 1] = generate_random_point(-h, h)
    t[h - 1, 0] = generate_random_point(-h, h)
    t[h - 1, h - 1] = generate_random_point(-h, h)

    reajusteur += t[0, 0] + t[0, h - 1] + t[h - 1, 0] + t[h - 1, h - 1]

    i = h - 1
    while i > 1:
        id = int(i / 2)
        for x in range(id, h, i):
            for y in range(id, h, i):
                moyenne = (t[x - id, y - id] + t[x - id, y + id] + t[x + id, y + id]
                           + t[x + id, y - id]) / 4
                t[x, y] = moyenne + generate_random_point(-id, id)
                reajusteur += t[x, y]
        decal = 0
        for x in range(0, h, id):
            if decal == 0:
                decal = id
            else:
                decal = 0
            for y in range(decal, h, i):
                somme = 0
                n = 0
                if x >= id:
                    somme = somme + t[x - id, y]
                    n = n + 1
                if x + id < h:
                    somme = somme + t[x + id, y]
                    n = n + 1
                if y >= id:
                    somme = somme + t[x, y - id]
                    n = n + 1
                if y + id < h:
                    somme = somme + t[x, y + id]
                    n = n + 1
                t[x, y] = somme / n + generate_random_point(-id, id)
                reajusteur += t[x, y]
        i = id
    reajusteur = round(reajusteur / (h * h), 0)
    print(t)
    print(reajusteur)

    for x in range(0, h):
        for y in range(0, h):
            t[x, y] -= reajusteur

            if t[x, y] < -h or t[x, y] > h:
                return False, t

            if t[x, y] < -(h * 0.9):
                t[x, y] = -(h * 0.9)
            elif t[x, y] < -(h * 0.8):
                t[x, y] = -(h * 0.8)
            elif t[x, y] < -(h * 0.7):
                t[x, y] = -(h * 0.7)
            elif t[x, y] < -(h * 0.6):
                t[x, y] = -(h * 0.6)
            elif t[x, y] < -(h * 0.5):
                t[x, y] = -(h * 0.5)
            elif t[x, y] < -(h * 0.4):
                t[x, y] = -(h * 0.4)
            elif t[x, y] < -(h * 0.3):
                t[x, y] = -(h * 0.3)
            elif t[x, y] < -(h * 0.2):
                t[x, y] = -(h * 0.2)
            elif t[x, y] < -(h * 0.1):
                t[x, y] = -(h * 0.1)
            elif t[x, y] < (h * 0.0):
                t[x, y] = h * 0.0
            elif t[x, y] < (h * 0.1):
                t[x, y] = h * 0.1
            elif t[x, y] < (h * 0.2):
                t[x, y] = h * 0.2
            elif t[x, y] < (h * 0.3):
                t[x, y] = h * 0.3
            elif t[x, y] < (h * 0.4):
                t[x, y] = h * 0.4
            elif t[x, y] < (h * 0.5):
                t[x, y] = h * 0.5
            elif t[x, y] < (h * 0.6):
                t[x, y] = h * 0.6
            elif t[x, y] < (h * 0.7):
                t[x, y] = h * 0.7
            elif t[x, y] < (h * 0.8):
                t[x, y] = h * 0.8
            elif t[x, y] < (h * 0.9):
                t[x, y] = h * 0.9
            else:
                t[x, y] = h * 0.9

    print("All right :D")
    return True, t


if __name__ == "__main__":
    while True:
        checker, carte = create_carte(7)
        if checker:
            mp.imsave("carte.png", carte)
            break
