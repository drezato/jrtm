# pylint: disable=redefined-builtin, inconsistent-return-statements, too-many-boolean-expressions, missing-type-doc, consider-using-tuple, redefined-outer-name, raising-bad-type, use-set-for-membership, unused-import
import math
import random

from copy import deepcopy
from attrdict import AttrDict
from dataclasses import dataclass

import fantasynames as names
import matplotlib.pyplot as mp
from perlin_noise import PerlinNoise

from db.db import DB
from utils.common import timeout, retry
from ressources.map.donjon.donjon import Donjon

from biome import Biome
from instance import Instance, DONJONS
from ville.ville import CAPITAL, VILLE, VILLAGE


@dataclass
class Map:
    elevation = None
    moisture = None
    map_biome = None
    map_capital = None
    map_ville = None
    map_village = None
    map_donjons = None

    size_x: int = 100
    size_y: int = 100
    frequency: float = 1.0
    redistribution: float = 1.0

    instances = []
    biome_checker = AttrDict({})

    def __post_init__(self):
        self.main()

    def main(self):
        print("\nCreation de la carte\n", flush=True)
        self.creation_biome_map()
        if not self.creation_instance_map():
            return self.main()
        self.generation_instance()
        self.generation_batiment()

    def creation_biome_map(self):
        self._init_biome_checker()
        while not self._check_biome_checker():
            print("Creation de la carte procedurale d'elevation", flush=True)
            self.elevation = self._create_perlin_map()

            print("Creation de la carte procedurale de moisture", flush=True)
            self.moisture = self._create_perlin_map()

            print("Insertion des biomes", flush=True)
            self.map_biome = self._insert_biome()

            print("Verification si la generation de la carte est au norme\n", flush=True)

    @retry(4)
    def creation_instance_map(self):
        self.instances = []

        print("Insertion des capitals", flush=True)
        self.map_capital = self.insert_instance(
            old_carte=self.map_biome, distance=50, instance_type=CAPITAL, check_environment=True
        )

        print("\nInsertion des villes", flush=True)
        self.map_ville = self.insert_instance(
            old_carte=self.map_capital,
            quantity_instance=3,
            distance=15,
            bordure_min=0.2,
            bordure_max=0.8,
            instance_type=VILLE,
            check_environment=True,
        )

        print("\nInsertion des villages", flush=True)
        self.map_village = self.insert_instance(
            old_carte=self.map_ville,
            quantity_instance=12,
            distance=10,
            bordure_min=0.1,
            bordure_max=0.9,
            instance_type=VILLAGE,
        )

        print("\nInsertion des donjons", flush=True)
        self.map_donjons = self.insert_instance(
            old_carte=self.map_village,
            quantity_instance=300,
            distance=2,
            bordure_min=0.0,
            bordure_max=1.0,
            instance_type=DONJONS,
            check_biome=False,
        )

        return self.map_donjons

    def _check_biome_checker(self) -> bool:
        if not all(value for value in self.biome_checker.values()):
            self._init_biome_checker()
            return False
        return True

    def _init_biome_checker(self) -> None:
        self.biome_checker = AttrDict(
            {
                Biome.OCEAN_PROFOND.biome: False,
                Biome.FORET_MAGIQUE.biome: False,
                Biome.MONTAGNE_PROFONDE.biome: False,
            }
        )

    def _create_perlin_map(self):
        noise = PerlinNoise(octaves=6)
        noise2 = PerlinNoise(octaves=12)
        noise3 = PerlinNoise(octaves=24)

        map = []
        for x in range(self.size_x):
            row = []
            for y in range(self.size_y):
                nx = x / self.size_x - 0.5
                ny = y / self.size_y - 0.5

                # Octave + Frequence
                noise_val = 1 * noise([nx * self.frequency, ny * self.frequency])
                noise_val += 0.5 * noise2([nx * self.frequency, ny * self.frequency])
                noise_val += 0.25 * noise3([nx * self.frequency, ny * self.frequency])
                noise_val /= 1 + 0.5 + 0.25

                # To be in range [0, 1] and not [-0.5, 0.5]
                noise_val += 0.5

                # Redistribution
                elevation = math.pow(noise_val, self.redistribution)
                row.append(elevation)
            map.append(row)
        return map

    def _insert_biome(self):
        map_biome = []
        for x in range(self.size_x - 1):
            row = []
            for y in range(self.size_y - 1):
                row.append(self.biome(self.elevation[x][y], self.moisture[x][y]))
            map_biome.append(row)
        return map_biome

    def _check_environment_biome(self, biome: object, x: int, y: int) -> bool:
        """Vérifie si le contour du point x et y est du même biome."""
        # Si on est en bordure (y = 99 par exemple) et qu'on vérifie sur y+1, ca pose un problème
        try:
            if (
                self.map_biome[x + 1][y] != biome
                or self.map_biome[x][y + 1] != biome
                or self.map_biome[x - 1][y] != biome
                or self.map_biome[x][y - 1] != biome
                or self.map_biome[x - 1][y - 1] != biome
                or self.map_biome[x - 1][y + 1] != biome
                or self.map_biome[x + 1][y + 1] != biome
                or self.map_biome[x + 1][y - 1] != biome
            ):
                return False
        except IndexError:
            pass
        return True

    @staticmethod
    def _check_distance(elements, x: int, y: int, distance: int = 10) -> bool:
        """Vérifie la distance entre les différents éléments de la list.

        Arguments:
            elements (list[list[int]]): la liste de point [x, y] à laquel il ne faudra pas être à proximité
            x (int): coordonnée x du point à vérifier
            y (int): coordonnée y du point à vérifier
            distance (int): distance à laquel les coordonnées x et y ne devront pas s'approcher de la liste
        """
        for element in elements:
            diff_x = element.coordonne_x - x
            diff_y = element.coordonne_y - y
            if abs(diff_x) + abs(diff_y) < distance:
                return False
        return True

    @timeout(3)
    def insert_instance(
        self,
        old_carte: list[list[AttrDict]],
        quantity_instance: int = 1,
        bordure_min: float = 0.2,
        bordure_max: float = 0.8,
        distance: int = 10,
        instance_type=None,
        check_environment: bool = False,
        check_biome: bool = True,
        event_timer=None,
    ):
        """Une fonction générique qui permet de rajouter une instance sur la carte

        Arguments:
            old_carte (list[list[AttrDict]]): carte sur laquel on va rajouter les instances
            quantity_instance (int): nombre d'instance que l'on souhaite rajouter sur la carte
            bordure_max (float): bordures limites max de la carte à laquelle l'instance peut se trouver
            bordure_min (float): bordures limites min de la carte à laquelle l'instance peut se trouver
            distance (int): distance entre les instances (les instances en cours de création ET déjà creer)
            instance_type (Instance): type d'instance (Exemple Ville, DONJONS)
            check_environment (bool): vérifie si les cases autour de l'instance sont du même biome
            check_biome (bool): si True, répartie équitablement le nombre d'instance entre Plaine, Colline et Foret
            event_timer: drapeau du timeout, si il passe à true il faut arreter la fonction

        Note:
            bordures: Si la carte fait 100x100 pixel, et que les bordures sont de 0.2 et 0.8, l'instance
                      pourra être posé qu'à partir de la case [20, 20] jusqu'à [80, 80]
            check_environment: Permet de centrer une instance par rapport au biome, utile dans la création de
                      de capital qui ne se retrouvera pas au bord d'un biome ou dans un petit biome.

        Exemple:
            insert_instance(old_carte=self.map_capital, quantity_instance=3, distance=10, bordure_min=0.1, bordure_max=0.9,
                instance_type=Instance.VILLE, check_environment=True)
            Cette appel permet de rajouter à la carte avec des capitals de 3 nouvelles villes par biomes (9 en tout)
            et les villes seront globalement centré par rapport aux biomes
        """
        map_ville = deepcopy(old_carte)
        nb_total_instance = 0
        instance_colline = 0
        instance_foret = 0
        instance_plaine = 0
        while not all(v == quantity_instance for v in [instance_colline, instance_foret, instance_plaine]):
            rand_x = random.randint(round(bordure_min * self.size_x), round(bordure_max * self.size_x - 2))
            rand_y = random.randint(round(bordure_min * self.size_y), round(bordure_max * self.size_y - 2))

            if (
                old_carte[rand_x][rand_y] == Biome.PLAINE
                and instance_plaine < quantity_instance
                and check_biome
            ):
                if self._check_environment_biome(Biome.PLAINE, rand_x, rand_y) or not check_environment:
                    if self._check_distance(self.instances, rand_x, rand_y, distance):
                        instance_plaine += 1
                        print(
                            f"{instance_type.instance} plaine : {instance_plaine} {[rand_x, rand_y]}",
                            flush=True,
                        )
                        instance_type.biome = map_ville[rand_x][rand_y].biome
                        instance_type.coordonne_x = rand_x
                        instance_type.coordonne_y = rand_y
                        map_ville[rand_x][rand_y] = deepcopy(instance_type)
                        self.instances.append(deepcopy(instance_type))

            elif (
                old_carte[rand_x][rand_y] == Biome.COLLINE
                and instance_colline < quantity_instance
                and check_biome
            ):
                if self._check_environment_biome(Biome.COLLINE, rand_x, rand_y) or not check_environment:
                    if self._check_distance(self.instances, rand_x, rand_y, distance):
                        instance_colline += 1
                        print(
                            f"{instance_type.instance} colline : {instance_colline} {[rand_x, rand_y]}",
                            flush=True,
                        )
                        instance_type.biome = map_ville[rand_x][rand_y].biome
                        instance_type.coordonne_x = rand_x
                        instance_type.coordonne_y = rand_y
                        map_ville[rand_x][rand_y] = deepcopy(instance_type)
                        self.instances.append(deepcopy(instance_type))

            elif (
                old_carte[rand_x][rand_y] == Biome.FORET
                and instance_foret < quantity_instance
                and check_biome
            ):
                if self._check_environment_biome(Biome.FORET, rand_x, rand_y) or not check_environment:
                    if self._check_distance(self.instances, rand_x, rand_y, distance):
                        instance_foret += 1
                        print(
                            f"{instance_type.instance} foret : {instance_foret} {[rand_x, rand_y]}",
                            flush=True,
                        )
                        instance_type.biome = map_ville[rand_x][rand_y].biome
                        instance_type.coordonne_x = rand_x
                        instance_type.coordonne_y = rand_y
                        map_ville[rand_x][rand_y] = deepcopy(instance_type)
                        self.instances.append(deepcopy(instance_type))

            if not check_biome:
                if self._check_distance(self.instances, rand_x, rand_y, distance):
                    nb_total_instance += 1
                    print(f"{instance_type.instance} : {nb_total_instance} {[rand_x, rand_y]}", flush=True)
                    instance_type.biome = map_ville[rand_x][rand_y].biome
                    instance_type.coordonne_x = rand_x
                    instance_type.coordonne_y = rand_y
                    map_ville[rand_x][rand_y] = deepcopy(instance_type)
                    self.instances.append(deepcopy(instance_type))
                if nb_total_instance == quantity_instance:
                    break

            if event_timer.is_set():
                break
        return map_ville

    def biome(self, elevation: float, moisture: float) -> object:
        if elevation < 0.22:
            self.biome_checker[Biome.MONTAGNE_PROFONDE.biome] = True
            return Biome.MONTAGNE_PROFONDE

        if elevation < 0.33:
            return Biome.MONTAGNE

        if elevation < 0.43:
            return Biome.COLLINE

        if elevation < 0.63:
            if moisture < 0.50:
                return Biome.PLAINE
            if moisture < 0.75:
                return Biome.FORET
            if moisture >= 0.75:
                self.biome_checker[Biome.FORET_MAGIQUE.biome] = True
                return Biome.FORET_MAGIQUE

        if elevation < 0.64:
            # if moisture > 0.3:
            return Biome.SABLE

        if elevation < 0.7:
            # if moisture > 0.3:
            return Biome.MER

        if elevation < 0.80:
            # if moisture > 0.3:
            return Biome.OCEAN

        self.biome_checker[Biome.OCEAN_PROFOND.biome] = True
        return Biome.OCEAN_PROFOND

    def to_graphical_map(self, carte: list[list[AttrDict]]) -> list[list[AttrDict]]:
        graphical_carte = []
        for x in range(self.size_x - 1):
            row = []
            for y in range(self.size_y - 1):
                row.append(carte[x][y].rgba)
            graphical_carte.append(row)
        return graphical_carte

    def generation_instance(self):
        all_donjon = []
        all_ville = []
        for instance in self.instances:
            instance.nom = generation_names(biome=instance.biome)
            if instance.type == "donjon":
                list_donjon = Donjon.list_donjon_biome(instance.biome)
                donjon = list_donjon[random.randint(0, len(list_donjon) - 1)]
                donjon_taille = random.randint(donjon["taille_min"], donjon["taille_max"])
                instance.taille = donjon_taille
                # print(instance, flush=True)
                # print(donjon, flush=True)
                all_donjon.append(
                    [
                        donjon["id"],
                        instance.nom,
                        instance.coordonne_x,
                        instance.coordonne_y,
                        donjon_taille,
                        instance.biome,
                    ]
                )

            elif instance.type == "ville":
                # print(instance, flush=True)
                all_ville.append(
                    [
                        instance.nom,
                        instance.instance,
                        instance.coordonne_x,
                        instance.coordonne_y,
                        instance.taille,
                        instance.biome,
                    ]
                )
            else:
                raise "Erreur instance non valide"
            print("\n")

        # DB().insert_multiple(
        #     "gen_donjon",
        #     all_donjon
        # )
        #
        # DB().insert_multiple(
        #     "gen_ville",
        #     all_ville
        # )

    def generation_batiment(self):
        villes = DB().select(table="gen_ville")
        batiments = DB().select(table="config_batiment")
        for ville in villes:
            print(ville)
            for x in range(0, ville["taille"]):
                for y in range(0, ville["taille"]):
                    rand_batiment = random.randint(0, len(batiments) - 1)
                    batiment = batiments[rand_batiment]
                    batiment["nom"] = generation_names(biome=ville["biome"])
                    print(f"[{x}:{y}] : {batiment}")
            print("\n")


def generation_names(biome):
    # print("hobbit = " + names.hobbit())
    # print("dwarf = " + names.dwarf())
    # print("elf = " + names.elf())
    # print("human = " + names.human())

    if biome in ["Colline", "Montagne", "Montagne profonde"]:
        name = names.dwarf()
    elif biome in ["Foret", "Foret magique"]:
        name = names.elf()
    else:
        name = names.hobbit()

    return name.split()[1]


if __name__ == "__main__":
    carte = Map(size_x=100, size_y=100, frequency=1, redistribution=1.0)
    # print(carte.map_donjons)
    # print(carte.instances, flush=True)

    # mp.imshow(carte.elevation, cmap='gray')
    # mp.imsave("carte.png", carte.elevation, cmap='gray')
    # mp.imsave("carte_biome.png", carte.map_biome)
    # mp.show()

    # mp.imshow(carte.to_graphical_map(carte.map_biome))
    # mp.show()

    # mp.imshow(carte.elevation)
    # mp.show()
    #
    # mp.imshow(carte.to_graphical_map(carte.map_biome))
    # mp.show()
    #
    # mp.imshow(carte.to_graphical_map(carte.map_capital))
    # mp.show()
    #
    # mp.imshow(carte.to_graphical_map(carte.map_ville))
    # mp.show()
    #
    # mp.imshow(carte.to_graphical_map(carte.map_village))
    # mp.show()

    mp.imshow(carte.to_graphical_map(carte.map_donjons))
    mp.show()
