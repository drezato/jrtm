from attrdict import AttrDict


class Biome:
    OCEAN = AttrDict({"biome": "Ocean", "rgba": (0, 0, 255)})
    OCEAN_PROFOND = AttrDict({"biome": "Ocean profond", "rgba": (0, 0, 170)})
    MER = AttrDict({"biome": "Mer", "rgba": (50, 130, 255)})
    SABLE = AttrDict({"biome": "Sable", "rgba": (255, 228, 0)})
    PLAINE = AttrDict({"biome": "Plaine", "rgba": (60, 179, 113)})
    FORET = AttrDict({"biome": "Foret", "rgba": (40, 99, 49)})
    FORET_MAGIQUE = AttrDict({"biome": "Foret magique", "rgba": (130, 60, 130)})
    COLLINE = AttrDict({"biome": "Colline", "rgba": (200, 200, 200)})
    MONTAGNE = AttrDict({"biome": "Montagne", "rgba": (90, 90, 90)})
    MONTAGNE_PROFONDE = AttrDict({"biome": "Montagne profonde", "rgba": (30, 30, 30)})
