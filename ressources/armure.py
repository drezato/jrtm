from dataclasses import dataclass


@dataclass
class Armure:
    nom: str = None
    cout: int = 0
    poids: int = 0

    bouclier_type: str = None
    jambiere_type: str = None
    brassiere_type: str = None
    casque_type: str = None
