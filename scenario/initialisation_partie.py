import os

from scenario.creation_personnage import CreateCharacter


class InitialisationPartie:
    INITIALISATION_PARTIE = [
        "Hey camarade, pour commencer la partie, press entrer :D",
        "Dans un monde lointain où les légendes sont encore vivantes, où la magie est réelle et où les créatures "
        "fantastiques peuplent encore les forêts et les montagnes, un mal ancestral se réveille. Depuis des siècles, "
        "un ancien et puissant sortilège maintenait prisonnier un démon aux pouvoirs dévastateurs, mais maintenant, "
        "celui-ci s'affaiblit. Les présages funestes se multiplient, les créatures monstrueuses apparaissent de plus "
        "en plus fréquemment et les rumeurs de complots sombres se répandent.",
        "Vous devrez explorer des terres "
        "sauvages, des cités anciennes et des lieux magiques, combattre des créatures fantastiques et des sorciers "
        "maléfiques, résoudre des énigmes complexes et choisir votre propre voie dans un monde rempli de choix et de "
        "conséquences. Votre histoire sera influencée par les décisions que vous prenez et les alliances que vous "
        "formez. Vous devrez faire des choix difficiles, mais à chaque étape de votre aventure, vous vous "
        "rapprocherez de votre but final : sauver le monde et devenir un véritable héros légendaire. ",
    ]

    def __init__(self):
        os.system("clear")

        for i in self.INITIALISATION_PARTIE:
            input(f"{i}\n")

        CreateCharacter()
