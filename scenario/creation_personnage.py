from typing import Union

from utils.dice import dice_100
from utils.checker_input import input_string, input_int

from ressources.creature.langue.langue import Langue
from ressources.creature.personnage.competence import CompetencePersonnage
from ressources.creature.personnage.magie_personnage import MagiePersonnage
from ressources.creature.personnage.caracteristique import CaracteristiqueCreature

from ressources.creature.personnage.personnage_joueur.race.nain import Nain
from ressources.creature.personnage.personnage_joueur.race.elfenoldor import ElfeNoldor
from ressources.creature.personnage.personnage_joueur.personnage_joueur import PersonnageJoueur


class CreateCharacter:
    NOM = "Donne moi ton nom jeune guerrier\n"

    RACES = [Nain, ElfeNoldor]

    RACE = "Race ?"
    TAILLE = "Taille ?\n"
    AGE = "Age ?\n"
    POIDS = "Poids ?\n"
    CHEVEUX = "Cheveux ?\n"
    YEUX = "Yeux ?\n"

    def __init__(self):
        self.player: Union[PersonnageJoueur, None] = None
        self.init_identite()
        self.init_sort()
        self.init_caracteristique()
        self.init_langue()
        self.utilisation_pts_histoire()
        self.init_comp()

        print(self.player)

    def init_identite(self):
        nom = input_string(self.NOM)

        print("Choississez votre race:")
        count = 0
        for race in self.RACES:
            print(f"[{count}] {race.race_name}")
            count += 1
        race = self.RACES[input_int(minimal=0, maximal=count - 1)]

        taille = input_int(self.TAILLE)
        age = input_int(self.AGE)
        poids = input_int(self.POIDS)
        cheveux = input_string(self.CHEVEUX)
        yeux = input_string(self.YEUX)

        self.player = race(nom=nom, taille=taille, age=age, poids=poids, cheveux=cheveux, yeux=yeux)
        # self.player = ElfeNoldor(nom="nom", taille="32", age="32", poids="32", cheveux="tete", yeux="tete")

    def init_caracteristique(self):
        print("Determination des caracteristiques \n")
        self.player.caracteristiques.force.value = dice_100(11)
        self.player.caracteristiques.agilite.value = dice_100(11)
        self.player.caracteristiques.constitution.value = dice_100(11)
        self.player.caracteristiques.intelligence.value = dice_100(11)
        self.player.caracteristiques.intuition.value = dice_100(11)
        self.player.caracteristiques.presence.value = dice_100(11)

        print(self.player.caracteristiques)
        print("")

    def init_sort(self):
        if self.player.chance_obtenir_liste_sort_pourcentage != 0:
            print(MagiePersonnage.PRETTY_CHOOSE_TXT)
            self.player.magie.upgrade_attribute_with_selection_point(
                points=self.player.chance_obtenir_liste_sort_pourcentage
            )
            print("")

    def init_langue(self):
        if self.player.degres_langage_additionnel != 0:
            print(Langue.PRETTY_CHOOSE_TXT)
            self.player.langue.upgrade_attribute_with_selection_point(
                points=self.player.degres_langage_additionnel
            )
            print("")

    def utilisation_pts_histoire(self):
        while self.player.points_histoire > 0:
            print(
                f"Vous avez {self.player.points_histoire} points d'histoires, "
                f"Ce qui vous permet de commencer avec des avantages \n"
                f"[0] Apprentissage parfait d'un language \n"
                f"[1] Augmentation de la valeur d'une caracteristique de 2 \n"
                f"[2] Augmentation de la valeur de trois caracteristiques de 1 \n"
                f"[3] Augmentation d'une compétence de 2 degrés (+10 point de degrés) \n"
                f"[4] Augmentation de trois compétence de 1 degré (+5 point de degrés) \n"
                f"[5] Gagne aléatoirement un montant de pièce d'or \n"
                f"[6] Apprentissage parfait d'une list de sort \n"
                # f"[7] Acquisition d'une habilité spéciale aléatoire \n"
                # f"[8] Acquisition d'un object spécial \n"
            )
            my_input = input_int(minimal=0, maximal=8)
            if my_input == 0:
                self.player.langue.upgrade_attribute_without_selection_point(points=5)
            elif my_input == 1:
                print(CaracteristiqueCreature.PRETTY_CHOOSE_TXT)
                self.player.caracteristiques.upgrade_attribute_without_selection_point(points=2)
            elif my_input == 2:
                print(CaracteristiqueCreature.PRETTY_CHOOSE_TXT)
                self.player.caracteristiques.upgrade_attribute_with_selection_point(points=3)
            elif my_input == 3:
                print(CompetencePersonnage.PRETTY_CHOOSE_TXT)
                self.player.competences.upgrade_attribute_without_selection_point(points=2)
            elif my_input == 4:
                print(CompetencePersonnage.PRETTY_CHOOSE_TXT)
                self.player.competences.upgrade_attribute_with_selection_point(points=3)
            elif my_input == 5:
                self.player.add_piece(niveau=1000)
                self.player.print_actual_player_piece()
            elif my_input == 6:
                print(MagiePersonnage.PRETTY_CHOOSE_TXT)
                self.player.magie.upgrade_attribute_with_selection_point(points=100)

            self.player.points_histoire -= 1
            print("")

    def init_comp(self):
        print(CompetencePersonnage.PRETTY_CHOOSE_TXT)
        self.player.competences.upgrade_attribute_with_selection_point(points=15)
