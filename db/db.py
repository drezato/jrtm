# pylint: disable=broad-exception-caught
from typing import Union
import mysql.connector as sql
from dataclasses import dataclass
from functools import cached_property

from db.credential_db import HOST_NAME, USER_NAME, USER_PASSWORD, DATABASE


@dataclass
class DB:
    host_name: str = HOST_NAME
    user_name: str = USER_NAME
    user_password: str = USER_PASSWORD
    database: str = DATABASE

    @cached_property
    def db(self):
        print("Database connection successful")
        return sql.connect(
            host=self.host_name,
            user=self.user_name,
            password=self.user_password,
            database=self.database
        )

    def _open(self, verbose: bool = True) -> None:
        try:
            self.cursor = self.db.cursor(dictionary=verbose)
        except Exception as e:
            print("Database connection failed :'(")
            print(e)

    # def _close(self) -> None:
    #     self.db.close()
    #     self.db = None
    #     self.cursor = None

    def select(
        self, table: str, value: str = "*", where: str = None, verbose: bool = True
    ) -> Union[list, dict]:
        query = f"SELECT {value} FROM `{table}`"
        if where:
            query += f" WHERE {where}"

        try:
            self.cursor.execute(query)
            result = self.cursor.fetchall()
        except AttributeError:
            self._open(verbose)
            self.cursor.execute(query)
            result = self.cursor.fetchall()

        return result

    def insert(
        self, table: str, values: list = None, verbose: bool = True
    ):
        query = f"INSERT INTO `{table}` VALUES (NULL, "
        if values:
            for value in values:
                query += f"'{value}', "
            query = f"{query[:-2]}"
        query += ")"

        try:
            self.cursor.execute(query)
        except AttributeError:
            self._open(verbose)
            self.cursor.execute(query)
        self.db.commit()
        # print(query, flush=True)

    def insert_multiple(self, table: str, list_values: list[list] = None, verbose: bool = True):
        for values in list_values:
            query = f"INSERT INTO `{table}` VALUES (NULL, "
            if values:
                for value in values:
                    query += f"'{value}', "
                query = f"{query[:-2]}"
            query += "); "
            try:
                self.cursor.execute(query)
            except AttributeError:
                self._open(verbose)
                self.cursor.execute(query)
            self.cursor.stored_results()
        # print(query, flush=True)

        self.db.commit()
