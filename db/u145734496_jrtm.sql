-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 11 mars 2023 à 20:05
-- Version du serveur : 10.5.18-MariaDB-cll-lve
-- Version de PHP : 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `u145734496_jrtm`
--

-- --------------------------------------------------------

--
-- Structure de la table `biome`
--

CREATE TABLE `biome` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `biome`
--

INSERT INTO `biome` (`id`, `nom`) VALUES
(4, 'Colline'),
(2, 'Foret'),
(3, 'Foret magique'),
(8, 'Mer'),
(5, 'Montagne'),
(6, 'Montagne profonde'),
(9, 'Ocean'),
(10, 'Ocean profond'),
(1, 'Plaine'),
(7, 'Sable');

-- --------------------------------------------------------

--
-- Structure de la table `config_action`
--

CREATE TABLE `config_action` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `config_action`
--

INSERT INTO `config_action` (`id`, `nom`) VALUES
(1, 'Combattre'),
(2, 'Fuir'),
(3, 'Acheter'),
(4, 'Vendre'),
(5, 'Equiper'),
(6, 'Manger'),
(7, 'Boire'),
(8, 'Dormir'),
(9, 'Prier');

-- --------------------------------------------------------

--
-- Structure de la table `config_batiment`
--

CREATE TABLE `config_batiment` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sous_type` varchar(255) NOT NULL,
  `action_list` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `config_batiment`
--

INSERT INTO `config_batiment` (`id`, `type`, `sous_type`, `action_list`) VALUES
(1, 'magasin', 'arme', '3 4'),
(2, 'taverne', '', '3 8'),
(3, 'magasin', 'armure', '3 4'),
(4, 'magasin', 'plante', '3 4'),
(5, 'magasin', 'accessoire', '3 4'),
(6, 'maison', '', '8'),
(7, 'jardin', '', ''),
(8, 'place', '', ''),
(9, 'porte', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `config_donjon`
--

CREATE TABLE `config_donjon` (
  `id` int(255) NOT NULL,
  `name` text NOT NULL,
  `taille_min` int(255) NOT NULL,
  `taille_max` int(255) NOT NULL,
  `forme` text NOT NULL,
  `biome` text NOT NULL,
  `salle_boss` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `config_donjon`
--

INSERT INTO `config_donjon` (`id`, `name`, `taille_min`, `taille_max`, `forme`, `biome`, `salle_boss`) VALUES
(1, 'Moulin', 4, 6, 'linear', 'Plaine', 0),
(2, 'Moulin', 4, 6, 'linear', 'Foret', 0),
(3, 'Arbre enchante', 3, 3, 'carre', 'Foret', 0),
(4, 'Arbre enchante', 3, 3, 'carre', 'Foret magique', 0),
(5, 'Grotte', 2, 4, 'carre', 'Colline', 0),
(6, 'Grotte', 2, 4, 'carre', 'Montagne', 0),
(7, 'Caverne profonde', 5, 7, 'linear', 'Montagne profonde', 0),
(8, 'Caverne', 4, 8, 'linear', 'Montagne', 0),
(9, 'Bateau', 2, 3, 'linear', 'Mer', 0),
(10, 'Bateau', 2, 3, 'linear', 'Ocean', 0),
(11, 'Bateau', 2, 3, 'linear', 'Ocean profond', 0),
(13, 'Chateau ensable', 2, 5, 'carre', 'Sable', 0);

-- --------------------------------------------------------

--
-- Structure de la table `config_evenement`
--

CREATE TABLE `config_evenement` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `action_list` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `config_evenement`
--

INSERT INTO `config_evenement` (`id`, `name`, `description`, `action_list`) VALUES
(1, 'Apparition monstre', 'Un monstre apparait !', '1 2'),
(2, 'Apparition coffre', 'Un coffre se dévoile devant vous :)', '');

-- --------------------------------------------------------

--
-- Structure de la table `creature`
--

CREATE TABLE `creature` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `vitesse` varchar(255) NOT NULL,
  `taille` varchar(255) NOT NULL,
  `critique` varchar(255) NOT NULL,
  `nombre_apparition_min` int(11) NOT NULL,
  `nombre_apparition_max` int(11) NOT NULL,
  `type_arme` varchar(255) NOT NULL,
  `type_armure` varchar(255) NOT NULL,
  `base_offensif` int(11) NOT NULL,
  `base_defensif` int(11) NOT NULL,
  `point_de_vie_max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `creature`
--

INSERT INTO `creature` (`id`, `nom`, `vitesse`, `taille`, `critique`, `nombre_apparition_min`, `nombre_apparition_max`, `type_arme`, `type_armure`, `base_offensif`, `base_defensif`, `point_de_vie_max`) VALUES
(1, 'Lapin', 'LE', 'T', 'Nor', 1, 4, 'Pe', 'NE', 0, 0, 5),
(2, 'Rat', 'LE', 'T', 'Nor', 3, 7, 'Pe', 'NE', 5, 0, 7);

-- --------------------------------------------------------

--
-- Structure de la table `evenement_biome`
--

CREATE TABLE `evenement_biome` (
  `id` int(11) NOT NULL,
  `id_config_evenement` int(11) NOT NULL,
  `id_creature` int(11) NOT NULL,
  `id_biome` int(11) NOT NULL,
  `probabilite` int(11) NOT NULL,
  `delta_apparition` int(11) NOT NULL COMMENT 'Influence sur le nombre d''apparition de la créature'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evenement_biome`
--

INSERT INTO `evenement_biome` (`id`, `id_config_evenement`, `id_creature`, `id_biome`, `probabilite`, `delta_apparition`) VALUES
(1, 1, 1, 1, 30, 0),
(2, 1, 2, 2, 30, 2);

-- --------------------------------------------------------

--
-- Structure de la table `evenement_donjon`
--

CREATE TABLE `evenement_donjon` (
  `id` int(11) NOT NULL,
  `id_config_evenement` int(11) NOT NULL,
  `id_creature` int(11) NOT NULL,
  `nom_donjon` varchar(255) NOT NULL,
  `probabilite` int(11) NOT NULL,
  `delta_apparition` int(11) NOT NULL COMMENT 'Influence sur le nombre d''apparition de la créature'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evenement_donjon`
--

INSERT INTO `evenement_donjon` (`id`, `id_config_evenement`, `id_creature`, `nom_donjon`, `probabilite`, `delta_apparition`) VALUES
(1, 1, 1, 'Moulin', 30, 0),
(2, 1, 2, 'Moulin', 30, 2),
(3, 2, 0, 'Moulin', 70, 0);

-- --------------------------------------------------------

--
-- Structure de la table `gen_batiment`
--

CREATE TABLE `gen_batiment` (
  `id` int(11) NOT NULL,
  `id_ville` int(11) NOT NULL,
  `id_config_batiment` int(11) NOT NULL,
  `nom` int(11) NOT NULL,
  `description` int(11) NOT NULL,
  `proprietaire` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `gen_donjon`
--

CREATE TABLE `gen_donjon` (
  `id` int(11) NOT NULL,
  `id_config_donjon` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `coordonne_x` int(11) NOT NULL,
  `coordonne_y` int(11) NOT NULL,
  `taille` int(11) NOT NULL,
  `biome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `gen_donjon`
--

INSERT INTO `gen_donjon` (`id`, `id_config_donjon`, `nom`, `coordonne_x`, `coordonne_y`, `taille`, `biome`) VALUES
(683, 2, 'Shadowheart', 20, 43, 5, 'Foret'),
(684, 2, 'Gamonus', 1, 33, 4, 'Foret'),
(685, 8, 'Ironpike', 39, 17, 5, 'Montagne'),
(686, 1, 'Fishbiscuits', 81, 31, 6, 'Plaine'),
(687, 1, 'Oldmuffins', 87, 50, 4, 'Plaine'),
(688, 10, 'Berrysticks', 69, 39, 3, 'Ocean'),
(689, 1, 'Fishbuck', 56, 12, 5, 'Plaine'),
(690, 2, 'Greenglade', 59, 96, 6, 'Foret'),
(691, 8, 'Strongbreak', 70, 72, 6, 'Montagne'),
(692, 3, 'Cliffwhisper', 69, 20, 3, 'Foret'),
(693, 5, 'Dustbreak', 75, 10, 3, 'Colline'),
(694, 1, 'Brandybiscuits', 7, 70, 6, 'Plaine'),
(695, 9, 'Rumbletoe', 97, 17, 3, 'Mer'),
(696, 1, 'Hungrytoe', 60, 65, 6, 'Plaine'),
(697, 5, 'Shieldstrike', 49, 61, 4, 'Colline'),
(698, 2, 'Fiariath', 91, 47, 4, 'Foret'),
(699, 3, 'Gamonus', 86, 71, 3, 'Foret'),
(700, 2, 'Duskweave', 84, 16, 5, 'Foret'),
(701, 1, 'Fiddlescone', 29, 92, 4, 'Plaine'),
(702, 2, 'Willowsong', 60, 77, 6, 'Foret'),
(703, 1, 'Hopetum', 79, 92, 5, 'Plaine'),
(704, 1, 'Pricklebiscuits', 33, 53, 6, 'Plaine'),
(705, 5, 'Greatscream', 38, 28, 2, 'Colline'),
(706, 5, 'Ironscream', 18, 21, 2, 'Colline'),
(707, 5, 'Steelspear', 72, 32, 4, 'Colline'),
(708, 5, 'Dusthammer', 47, 9, 4, 'Colline'),
(709, 5, 'Grimspear', 15, 55, 3, 'Colline'),
(710, 1, 'Hopepipe', 28, 67, 6, 'Plaine'),
(711, 10, 'Jigglecakes', 76, 57, 2, 'Ocean'),
(712, 1, 'Rumbleburrow', 58, 47, 5, 'Plaine'),
(713, 10, 'Prickletart', 14, 71, 3, 'Ocean'),
(714, 3, 'Cemaine', 7, 54, 3, 'Foret'),
(715, 3, 'Brightblossom', 74, 34, 3, 'Foret'),
(716, 1, 'Jigglebelly', 34, 37, 5, 'Plaine'),
(717, 3, 'Feamuth', 66, 76, 3, 'Foret'),
(718, 2, 'Mistvale', 57, 39, 6, 'Foret'),
(719, 1, 'Longsticks', 96, 44, 4, 'Plaine'),
(720, 5, 'Proudblaze', 89, 33, 2, 'Colline'),
(721, 2, 'Reamuth', 62, 17, 4, 'Foret'),
(722, 1, 'Sneakycrust', 65, 57, 5, 'Plaine'),
(723, 1, 'Pricklefoot', 29, 24, 5, 'Plaine'),
(724, 3, 'Sagewood', 80, 26, 3, 'Foret'),
(725, 5, 'Gravelhelm', 96, 6, 2, 'Colline'),
(726, 1, 'Prickletart', 3, 12, 6, 'Plaine'),
(727, 2, 'Theraine', 42, 4, 4, 'Foret'),
(728, 1, 'Sneakythorp', 34, 61, 4, 'Plaine'),
(729, 2, 'Nightblossom', 85, 28, 5, 'Foret'),
(730, 3, 'Cliffgrove', 44, 57, 3, 'Foret'),
(731, 5, 'Strongbeard', 46, 20, 3, 'Colline'),
(732, 1, 'Fiddlecandles', 81, 94, 4, 'Plaine'),
(733, 1, 'Wimblepipe', 95, 64, 5, 'Plaine'),
(734, 3, 'Windvale', 37, 43, 3, 'Foret'),
(735, 5, 'Mountainmail', 90, 71, 2, 'Colline'),
(736, 1, 'Poppysticks', 89, 2, 6, 'Plaine'),
(737, 10, 'Purpletum', 95, 18, 3, 'Ocean'),
(738, 1, 'Hungryscone', 0, 83, 6, 'Plaine'),
(739, 2, 'Cliffgrove', 45, 46, 5, 'Foret'),
(740, 1, 'Rumblethorp', 40, 55, 4, 'Plaine'),
(741, 1, 'Crumbletum', 67, 13, 4, 'Plaine'),
(742, 1, 'Freshburrow', 59, 64, 5, 'Plaine'),
(743, 5, 'Shieldmead', 55, 84, 4, 'Colline'),
(744, 3, 'Stormwood', 68, 75, 3, 'Foret'),
(745, 2, 'Fynaine', 52, 21, 5, 'Foret'),
(746, 2, 'Ieluth', 98, 56, 4, 'Foret'),
(747, 9, 'Freshcandles', 40, 36, 3, 'Mer'),
(748, 5, 'Proudspear', 18, 19, 3, 'Colline'),
(749, 1, 'Fiddlemuffins', 56, 67, 4, 'Plaine'),
(750, 1, 'Hungrybiscuits', 2, 57, 4, 'Plaine'),
(751, 8, 'Strongscream', 89, 89, 4, 'Montagne'),
(752, 2, 'Greenshade', 56, 31, 5, 'Foret'),
(753, 3, 'Willowgrove', 91, 81, 3, 'Foret'),
(754, 1, 'Crumblestack', 19, 2, 4, 'Plaine'),
(755, 1, 'Rumblepipe', 57, 75, 4, 'Plaine'),
(756, 5, 'Dustbeard', 28, 96, 4, 'Colline'),
(757, 5, 'Shieldmace', 7, 84, 2, 'Colline'),
(758, 5, 'Dustgrip', 14, 63, 2, 'Colline'),
(759, 9, 'Smoketum', 47, 41, 2, 'Mer'),
(760, 9, 'Hungrytart', 29, 14, 2, 'Mer'),
(761, 5, 'Mountainbrew', 7, 78, 3, 'Colline'),
(762, 2, 'Amonus', 44, 45, 4, 'Foret'),
(763, 5, 'Stoutbreak', 13, 85, 4, 'Colline'),
(764, 9, 'Poppybelly', 78, 24, 2, 'Mer'),
(765, 3, 'Dewshade', 59, 73, 3, 'Foret'),
(766, 1, 'Rumbletoe', 8, 16, 5, 'Plaine'),
(767, 5, 'Gravelbrow', 56, 91, 3, 'Colline'),
(768, 5, 'Darkstrike', 4, 58, 2, 'Colline'),
(769, 3, 'Dreamheart', 76, 19, 3, 'Foret'),
(770, 10, 'Crumblepipe', 95, 26, 3, 'Ocean'),
(771, 1, 'Sneakythorp', 72, 41, 4, 'Plaine'),
(772, 1, 'Applefeet', 55, 58, 4, 'Plaine'),
(773, 1, 'Brandycrust', 96, 62, 4, 'Plaine'),
(774, 5, 'Stonebeard', 41, 83, 3, 'Colline'),
(775, 1, 'Sneakyburrow', 23, 9, 5, 'Plaine'),
(776, 7, 'Steelblaze', 84, 92, 6, 'Montagne profonde'),
(777, 1, 'Shortbum', 8, 18, 4, 'Plaine'),
(778, 5, 'Shieldblaze', 73, 76, 3, 'Colline'),
(779, 2, 'Dewgrove', 14, 38, 6, 'Foret'),
(780, 1, 'Freshpipe', 53, 54, 4, 'Plaine'),
(781, 1, 'Fishfoot', 47, 63, 5, 'Plaine'),
(782, 2, 'Dewwing', 86, 77, 5, 'Foret'),
(783, 1, 'Berryfinger', 28, 81, 6, 'Plaine'),
(784, 1, 'Fiddlewort', 20, 94, 4, 'Plaine'),
(785, 9, 'Hopetum', 14, 30, 2, 'Mer'),
(786, 2, 'Eironus', 79, 76, 6, 'Foret'),
(787, 6, 'Stoutbrew', 2, 93, 3, 'Montagne'),
(788, 2, 'Stormblossom', 60, 0, 5, 'Foret'),
(789, 11, 'Buttercandles', 96, 22, 2, 'Ocean profond'),
(790, 3, 'Einonus', 35, 22, 3, 'Foret'),
(791, 1, 'Shortbuck', 66, 55, 4, 'Plaine'),
(792, 1, 'Fiddlebelly', 81, 54, 4, 'Plaine'),
(793, 3, 'Thaviel', 97, 54, 3, 'Foret'),
(794, 1, 'Berrytart', 91, 49, 4, 'Plaine'),
(795, 1, 'Applecrust', 6, 19, 5, 'Plaine'),
(796, 5, 'Mountainblaze', 86, 45, 3, 'Colline'),
(797, 9, 'Milkbuck', 12, 70, 3, 'Mer'),
(798, 2, 'Stormweave', 84, 81, 4, 'Foret'),
(799, 1, 'Fiddlebelly', 61, 68, 4, 'Plaine'),
(800, 1, 'Shortcakes', 95, 90, 6, 'Plaine'),
(801, 1, 'Brandybum', 37, 4, 5, 'Plaine'),
(802, 9, 'Freshstack', 64, 2, 3, 'Mer'),
(803, 9, 'Sweetcandles', 24, 12, 2, 'Mer'),
(804, 1, 'Wimblethorp', 67, 9, 6, 'Plaine'),
(805, 6, 'Stronghelm', 6, 93, 2, 'Montagne'),
(806, 5, 'Stonebrew', 56, 86, 2, 'Colline'),
(807, 1, 'Fishthorp', 70, 63, 4, 'Plaine'),
(808, 1, 'Fishtreats', 26, 69, 6, 'Plaine'),
(809, 2, 'Shadowheart', 79, 72, 5, 'Foret'),
(810, 5, 'Gravelblaze', 49, 7, 2, 'Colline'),
(811, 1, 'Milktum', 36, 95, 5, 'Plaine'),
(812, 1, 'Pricklecakes', 91, 32, 5, 'Plaine'),
(813, 1, 'Wimblefinger', 12, 16, 6, 'Plaine'),
(814, 6, 'Steelgrip', 88, 8, 3, 'Montagne'),
(815, 1, 'Creambuck', 3, 52, 5, 'Plaine'),
(816, 5, 'Stonehammer', 90, 38, 3, 'Colline'),
(817, 3, 'Mistgrove', 49, 48, 3, 'Foret'),
(818, 6, 'Proudbrow', 10, 55, 3, 'Montagne'),
(819, 1, 'Applecakes', 66, 3, 4, 'Plaine'),
(820, 3, 'Iemaine', 39, 33, 3, 'Foret'),
(821, 10, 'Poppyburrow', 19, 10, 3, 'Ocean'),
(822, 3, 'Highwing', 93, 81, 3, 'Foret'),
(823, 3, 'Windthorn', 7, 31, 3, 'Foret'),
(824, 2, 'Einaire', 91, 77, 4, 'Foret'),
(825, 2, 'Dreamthorn', 88, 22, 4, 'Foret'),
(826, 2, 'Sageshade', 79, 66, 5, 'Foret'),
(827, 5, 'Deepgrip', 87, 68, 4, 'Colline'),
(828, 10, 'Hungrythorp', 74, 55, 3, 'Ocean'),
(829, 9, 'Pricklecakes', 63, 0, 2, 'Mer'),
(830, 1, 'Hungrytum', 49, 55, 5, 'Plaine'),
(831, 5, 'Stoutpick', 92, 67, 3, 'Colline'),
(832, 5, 'Stouthelm', 55, 2, 2, 'Colline'),
(833, 2, 'Highwood', 42, 76, 4, 'Foret'),
(834, 1, 'Smokescone', 1, 17, 6, 'Plaine'),
(835, 2, 'Ailuth', 77, 42, 5, 'Foret'),
(836, 5, 'Battlemead', 76, 4, 4, 'Colline'),
(837, 8, 'Proudgrip', 5, 39, 8, 'Montagne'),
(838, 1, 'Fiddlecandles', 67, 87, 5, 'Plaine'),
(839, 2, 'Ceneus', 45, 86, 4, 'Foret'),
(840, 1, 'Pricklefoot', 13, 96, 6, 'Plaine'),
(841, 1, 'Brandybiscuits', 50, 80, 4, 'Plaine'),
(842, 3, 'Aironus', 18, 0, 3, 'Foret'),
(843, 9, 'Wimblemuffins', 83, 58, 2, 'Mer'),
(844, 9, 'Fiddlestack', 71, 44, 2, 'Mer'),
(845, 3, 'Greenhunter', 64, 82, 3, 'Foret'),
(846, 9, 'Tickleweed', 96, 95, 2, 'Mer'),
(847, 1, 'Wimbletart', 26, 79, 5, 'Plaine'),
(848, 1, 'Longtart', 20, 52, 4, 'Plaine'),
(849, 3, 'Anion', 40, 7, 3, 'Foret'),
(850, 1, 'Sneakytart', 4, 51, 4, 'Plaine'),
(851, 2, 'Geaviel', 63, 47, 4, 'Foret'),
(852, 5, 'Darkpick', 0, 2, 3, 'Colline'),
(853, 8, 'Heavyfury', 71, 70, 7, 'Montagne'),
(854, 4, 'Brightgrass', 62, 26, 3, 'Foret magique'),
(855, 5, 'Strongpick', 7, 39, 3, 'Colline'),
(856, 1, 'Berryfinger', 82, 5, 4, 'Plaine'),
(857, 1, 'Butterwort', 32, 97, 6, 'Plaine'),
(858, 1, 'Oldscone', 36, 54, 4, 'Plaine'),
(859, 2, 'Hawkroot', 46, 22, 5, 'Foret'),
(860, 9, 'Rumblethorp', 46, 42, 3, 'Mer'),
(861, 1, 'Pricklemuffins', 22, 93, 5, 'Plaine'),
(862, 1, 'Fiddletoe', 50, 82, 6, 'Plaine'),
(863, 2, 'Greenglade', 13, 74, 6, 'Foret'),
(864, 9, 'Shortcakes', 92, 22, 2, 'Mer'),
(865, 5, 'Grimmail', 18, 58, 3, 'Colline'),
(866, 6, 'Stonemail', 39, 12, 3, 'Montagne'),
(867, 5, 'Darkblaze', 7, 59, 4, 'Colline'),
(868, 2, 'Brightgrove', 20, 77, 5, 'Foret'),
(869, 5, 'Stonespear', 28, 61, 3, 'Colline'),
(870, 1, 'Berrythorp', 79, 11, 6, 'Plaine'),
(871, 1, 'Milkweed', 50, 51, 4, 'Plaine'),
(872, 10, 'Brandywort', 57, 18, 3, 'Ocean'),
(873, 5, 'Grimbrow', 6, 33, 3, 'Colline'),
(874, 3, 'Sunroot', 56, 80, 3, 'Foret'),
(875, 13, 'Rumblesticks', 50, 73, 5, 'Sable'),
(876, 1, 'Wimblesticks', 22, 0, 6, 'Plaine'),
(877, 9, 'Jigglethorp', 15, 40, 3, 'Mer'),
(878, 5, 'Mountainbrew', 92, 37, 2, 'Colline'),
(879, 3, 'Sunwhisper', 39, 40, 3, 'Foret'),
(880, 1, 'Oldcakes', 95, 4, 6, 'Plaine'),
(881, 2, 'Highvale', 56, 38, 4, 'Foret'),
(882, 9, 'Ticklecrust', 51, 18, 2, 'Mer'),
(883, 1, 'Ticklesticks', 95, 46, 5, 'Plaine'),
(884, 5, 'Greatpick', 33, 70, 3, 'Colline'),
(885, 2, 'Garaire', 44, 10, 6, 'Foret'),
(886, 3, 'Gwaliath', 79, 21, 3, 'Foret'),
(887, 5, 'Stoutbrow', 36, 7, 2, 'Colline'),
(888, 2, 'Ieviel', 68, 89, 5, 'Foret'),
(889, 4, 'Duskvale', 55, 5, 3, 'Foret magique'),
(890, 5, 'Proudmace', 2, 3, 4, 'Colline'),
(891, 2, 'Duskheart', 56, 4, 5, 'Foret'),
(892, 3, 'Enion', 78, 34, 3, 'Foret'),
(893, 9, 'Fiddlecakes', 59, 7, 3, 'Mer'),
(894, 1, 'Hopecandles', 48, 27, 5, 'Plaine'),
(895, 3, 'Ieliath', 15, 19, 3, 'Foret'),
(896, 5, 'Darkmead', 54, 96, 3, 'Colline'),
(897, 1, 'Hopetum', 33, 67, 5, 'Plaine'),
(898, 5, 'Heavybrew', 48, 32, 4, 'Colline'),
(899, 2, 'Mistshade', 49, 68, 6, 'Foret'),
(900, 1, 'Fishtum', 26, 3, 5, 'Plaine'),
(901, 5, 'Mountainmead', 16, 21, 3, 'Colline'),
(902, 1, 'Bubbletart', 23, 78, 6, 'Plaine'),
(903, 1, 'Hopebum', 45, 33, 5, 'Plaine'),
(904, 1, 'Bubblefeet', 2, 16, 5, 'Plaine'),
(905, 1, 'Wimblebiscuits', 34, 20, 4, 'Plaine'),
(906, 13, 'Oldcakes', 95, 30, 3, 'Sable'),
(907, 6, 'Heavyrage', 39, 14, 4, 'Montagne'),
(908, 3, 'Gweviel', 4, 48, 3, 'Foret'),
(909, 1, 'Rumbleburrow', 74, 19, 4, 'Plaine'),
(910, 9, 'Shortfinger', 15, 8, 3, 'Mer'),
(911, 1, 'Bubblecandles', 69, 11, 6, 'Plaine'),
(912, 9, 'Brandypipe', 60, 17, 3, 'Mer'),
(913, 9, 'Milksticks', 61, 60, 3, 'Mer'),
(914, 5, 'Stonestrike', 69, 74, 4, 'Colline'),
(915, 3, 'Greensong', 0, 50, 3, 'Foret'),
(916, 3, 'Greenroot', 34, 30, 3, 'Foret'),
(917, 3, 'Hawkhunter', 65, 88, 3, 'Foret'),
(918, 1, 'Honeyscone', 68, 44, 4, 'Plaine'),
(919, 6, 'Darkhelm', 3, 91, 3, 'Montagne'),
(920, 5, 'Ironstrike', 21, 96, 2, 'Colline'),
(921, 5, 'Deepblaze', 20, 61, 4, 'Colline'),
(922, 9, 'Wimblesticks', 54, 17, 3, 'Mer'),
(923, 1, 'Berryscone', 70, 86, 5, 'Plaine'),
(924, 1, 'Creamtart', 54, 49, 6, 'Plaine'),
(925, 1, 'Freshbelly', 36, 61, 4, 'Plaine'),
(926, 5, 'Gravelmead', 92, 73, 3, 'Colline'),
(927, 2, 'Cloudweave', 82, 46, 4, 'Foret'),
(928, 6, 'Proudblaze', 55, 90, 3, 'Montagne'),
(929, 1, 'Crumblestack', 14, 97, 5, 'Plaine'),
(930, 5, 'Shieldfury', 28, 57, 4, 'Colline'),
(931, 1, 'Wimblescone', 32, 61, 4, 'Plaine'),
(932, 2, 'Airaine', 50, 46, 4, 'Foret'),
(933, 1, 'Longbelly', 53, 23, 5, 'Plaine'),
(934, 5, 'Greatbrow', 2, 59, 2, 'Colline'),
(935, 1, 'Hungrycakes', 73, 17, 6, 'Plaine'),
(936, 5, 'Stonefury', 82, 90, 2, 'Colline'),
(937, 1, 'Honeymuffins', 91, 84, 6, 'Plaine'),
(938, 1, 'Butterthorp', 82, 55, 4, 'Plaine'),
(939, 1, 'Tickletart', 87, 18, 6, 'Plaine'),
(940, 1, 'Hungrysticks', 0, 10, 5, 'Plaine'),
(941, 1, 'Sweetcakes', 83, 84, 6, 'Plaine'),
(942, 5, 'Stonerage', 21, 66, 3, 'Colline'),
(943, 10, 'Brandycrust', 45, 41, 2, 'Ocean'),
(944, 1, 'Fishmuffins', 85, 53, 6, 'Plaine'),
(945, 6, 'Stronggrip', 6, 96, 4, 'Montagne'),
(946, 2, 'Feaneus', 58, 25, 4, 'Foret'),
(947, 5, 'Ironstrike', 33, 64, 2, 'Colline'),
(948, 10, 'Shortbuck', 64, 65, 3, 'Ocean'),
(949, 2, 'Dewwing', 10, 27, 6, 'Foret'),
(950, 1, 'Smokeweed', 30, 82, 4, 'Plaine'),
(951, 3, 'Thalaine', 69, 90, 3, 'Foret'),
(952, 13, 'Crumblestack', 16, 13, 4, 'Sable'),
(953, 1, 'Brandytoe', 8, 22, 5, 'Plaine'),
(954, 1, 'Berryweed', 88, 56, 5, 'Plaine'),
(955, 1, 'Sweetbelly', 50, 75, 6, 'Plaine'),
(956, 1, 'Sweettart', 67, 85, 4, 'Plaine'),
(957, 5, 'Dustmead', 76, 30, 2, 'Colline'),
(958, 1, 'Hopesticks', 12, 21, 5, 'Plaine'),
(959, 3, 'Sunweave', 83, 26, 3, 'Foret'),
(960, 1, 'Longcakes', 71, 19, 4, 'Plaine'),
(961, 4, 'Theviel', 60, 90, 3, 'Foret magique'),
(962, 2, 'Cloudwhisper', 60, 38, 6, 'Foret'),
(963, 1, 'Crumbleweed', 19, 6, 6, 'Plaine'),
(964, 6, 'Mountainrage', 68, 28, 3, 'Montagne'),
(965, 3, 'Shadowbark', 41, 8, 3, 'Foret'),
(966, 5, 'Grimbrew', 40, 9, 4, 'Colline'),
(967, 1, 'Crumblecakes', 72, 93, 6, 'Plaine'),
(968, 1, 'Hungrybelly', 65, 15, 4, 'Plaine'),
(969, 2, 'Sagewood', 34, 8, 6, 'Foret'),
(970, 5, 'Gravelstrike', 2, 65, 4, 'Colline'),
(971, 1, 'Bubbletum', 75, 90, 6, 'Plaine'),
(972, 1, 'Wimblescone', 29, 30, 6, 'Plaine'),
(973, 5, 'Proudpike', 93, 7, 3, 'Colline'),
(974, 2, 'Thamaire', 81, 70, 4, 'Foret'),
(975, 8, 'Greatcask', 3, 98, 4, 'Montagne'),
(976, 3, 'Eileus', 59, 4, 3, 'Foret'),
(977, 5, 'Darkpike', 29, 47, 4, 'Colline'),
(978, 1, 'Sweetbiscuits', 72, 14, 4, 'Plaine'),
(979, 2, 'Leviel', 68, 77, 6, 'Foret'),
(980, 5, 'Grimscream', 10, 61, 4, 'Colline'),
(981, 10, 'Smokeburrow', 48, 39, 2, 'Ocean'),
(982, 2, 'Greenwood', 37, 26, 5, 'Foret');

-- --------------------------------------------------------

--
-- Structure de la table `gen_ville`
--

CREATE TABLE `gen_ville` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `coordonne_x` int(11) NOT NULL,
  `coordonne_y` int(11) NOT NULL,
  `taille` int(11) NOT NULL,
  `biome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `list_sort`
--

CREATE TABLE `list_sort` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `list_sort`
--

INSERT INTO `list_sort` (`id`, `nom`, `type`) VALUES
(1, 'Les voies du feu', 'intelligence'),
(2, 'Les voies du froid', 'intelligence');

-- --------------------------------------------------------

--
-- Structure de la table `sort`
--

CREATE TABLE `sort` (
  `id` int(11) NOT NULL,
  `id_list_sort` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sort`
--

INSERT INTO `sort` (`id`, `id_list_sort`, `nom`, `type`, `niveau`) VALUES
(1, 1, 'Ebulition des liquides', 'F', 1),
(2, 1, 'Echauffement des solides', 'F', 2),
(3, 2, 'Congélation des liquides', 'F', 1),
(4, 2, 'Refroidissement des solides', 'F', 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `biome`
--
ALTER TABLE `biome`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nom` (`nom`);

--
-- Index pour la table `config_action`
--
ALTER TABLE `config_action`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `config_batiment`
--
ALTER TABLE `config_batiment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `config_donjon`
--
ALTER TABLE `config_donjon`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `config_evenement`
--
ALTER TABLE `config_evenement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `creature`
--
ALTER TABLE `creature`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evenement_biome`
--
ALTER TABLE `evenement_biome`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_creature` (`id_biome`),
  ADD KEY `nom_donjon` (`id_creature`);

--
-- Index pour la table `evenement_donjon`
--
ALTER TABLE `evenement_donjon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_creature` (`nom_donjon`),
  ADD KEY `nom_donjon` (`id_creature`);

--
-- Index pour la table `gen_batiment`
--
ALTER TABLE `gen_batiment`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `gen_donjon`
--
ALTER TABLE `gen_donjon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_config_donjon` (`id_config_donjon`);

--
-- Index pour la table `gen_ville`
--
ALTER TABLE `gen_ville`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `list_sort`
--
ALTER TABLE `list_sort`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sort`
--
ALTER TABLE `sort`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `biome`
--
ALTER TABLE `biome`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `config_action`
--
ALTER TABLE `config_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `config_batiment`
--
ALTER TABLE `config_batiment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `config_donjon`
--
ALTER TABLE `config_donjon`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `config_evenement`
--
ALTER TABLE `config_evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `creature`
--
ALTER TABLE `creature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `evenement_biome`
--
ALTER TABLE `evenement_biome`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `evenement_donjon`
--
ALTER TABLE `evenement_donjon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `gen_batiment`
--
ALTER TABLE `gen_batiment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `gen_donjon`
--
ALTER TABLE `gen_donjon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=983;

--
-- AUTO_INCREMENT pour la table `list_sort`
--
ALTER TABLE `list_sort`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `sort`
--
ALTER TABLE `sort`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
